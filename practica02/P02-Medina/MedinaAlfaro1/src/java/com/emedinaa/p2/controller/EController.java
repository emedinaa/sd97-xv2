/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emedinaa.p2.controller;

import com.emedinaa.p2.model.Evaluation;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author edmedina
 */
@Named(value = "operation")
@ApplicationScoped
public class EController {
    
    private final int A = 0;
    private final int B = 1;
    private final int C = 2;
    
    @Inject
    Evaluation evaluation;

    public int calculateH(){
        return calculate()/60;
    }
    
    public int calculateM(){
        return calculate()%60;
    }
       
    
    private int calculate() {
        int totalA = evaluation.getStudentsA() * timeBySubject(A);
        int totalB = evaluation.getStudentsB() * timeBySubject(B);
        int totalC = evaluation.getStudentsC() * timeBySubject(C);

        System.out.println("total A " + totalA);
        System.out.println("total B " + totalB);
        System.out.println("total C " + totalC);

        int total = totalA + totalB + totalC;
        System.out.println("total " + total);
        return total;
        
        /*int h = total / 60;
        int m = total % 60;

        System.out.println("h " + h);
        System.out.println("m " + m);*/
    }
    
  
    private int timeBySubject(int subject) {
        switch (subject) {
            case A:
                return 7;
            case B:
                return 9;
            case C:
                return 10;
            default:
                return 0;
        }
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }
    
    
    
}
