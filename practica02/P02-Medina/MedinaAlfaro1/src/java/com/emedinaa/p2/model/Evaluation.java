/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emedinaa.p2.model;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author edmedina
 */
@Named(value = "evaluation")
@ApplicationScoped
public class Evaluation {
    int studentsA, studentsB,studentsC;

    public int getStudentsA() {
        return studentsA;
    }

    public void setStudentsA(int studentsA) {
        this.studentsA = studentsA;
    }

    public int getStudentsB() {
        return studentsB;
    }

    public void setStudentsB(int studentsB) {
        this.studentsB = studentsB;
    }

    public int getStudentsC() {
        return studentsC;
    }

    public void setStudentsC(int studentsC) {
        this.studentsC = studentsC;
    }
    
    
    public void validateTotalStudent(FacesContext context, UIComponent comp,
			Object value) {

        System.out.println("validar campo de total de estudiantes");

        int students = (int) value;

        if (students <=0 || students > 35) {
            ((UIInput) comp).setValid(false);

            FacesMessage message = new FacesMessage(
                            "Campo inválido, debe ser mayor que cero y menor igual que 35");
            context.addMessage(comp.getClientId(context), message);

        }

    }

    
    public Evaluation() {
    }
    
    
}
