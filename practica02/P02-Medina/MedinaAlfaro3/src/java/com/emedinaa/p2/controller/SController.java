/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emedinaa.p2.controller;

import com.emedinaa.p2.model.Salary;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author edmedina
 */
@Named(value = "operation")
@ApplicationScoped
public class SController {
    
    private final int A = 0;
    private final int B = 1;
    private final int C = 2;
    
    @Inject
    Salary salary;
    
     public String  calculate(){
        double userSalary = salary.getHours()*rateByCategory(salary.getCategory()) ;
        double bonus= userSalary*bonusByYear(salary.getYear());
        double extra = bonus*extraByMonth(salary.getMonth());
        double total = userSalary +extra;
        System.out.println("salary  " + userSalary); //320
        System.out.println("bonus  " + bonus); //96
        System.out.println("extra  " + extra);//96x2 julio 1 , dic 2
        System.out.println("total  " + total);
        
        return "Salario : "+userSalary+ " | Gratificación : "+extra+ " | Total : "+total;
    }

    private int extraByMonth(int month){
        if(month>=12){ //december
            return 2;
        }else if(month >=7){//july
            return 1;
        }else{
            return 0;
        }
    }
      
    private double bonusByYear(int year){
        if(year>=0 && year<=2){
            return 0.3;
        }else if(year>=3 && year<=7){
            return 0.5;
        }else if(year>=8 && year<=10){
            return 0.7;
        }else {
            return 1.0;
        }
    }

    private double rateByCategory(int category){
        switch (category) {
            case A:
                return 40.00;
            case B:
                return 35.00;
            case C:
                return 30.00;
            default:
                return 00.00;
        }
    }
}
