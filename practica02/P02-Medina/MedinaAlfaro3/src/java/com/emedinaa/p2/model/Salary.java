/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emedinaa.p2.model;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author edmedina
 */
@Named(value = "salary")
@ApplicationScoped
public class Salary {
    
    int hours;
    int month;//7 12;// 1,2,3,4,5,6,7,8,9,10,11,12
    int category; // A B C
    int year;

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
    
}
