/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emedinaa.p2.controller;

import com.emedinaa.p2.model.Time;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author edmedina
 */
@Named(value = "operation")
@ApplicationScoped
public class TController {
    
    private final int DAY_IN_SECONDS = 24*3600;
    
    @Inject
    Time time;
     
    public String calculate(){
        
        int t1 = toSeconds(time.gethFirst(), time.getmFirst(), time.getsFirst());
        int t2 = toSeconds(time.gethSecond(), time.getmSecond(), time.getsSecond());

        System.out.println("t1 " + t1);
        System.out.println("t2 " + t2);
        if (t1 > t2) {
            return "Error : El primer tiempo debe ser menor que el segundo tiempo!";
        }

        int total = t2-t1;
        System.out.println("total " + total);
        int days = total/(DAY_IN_SECONDS); //86400
        double h = (total*1.0%(DAY_IN_SECONDS))/3600;
        double m = (h*60)%(60);
        double s = m%(60);

        System.out.println("days  " + days);
        System.out.println("h  " + convertToInt(h));
        System.out.println("m  " + convertToInt(m));
        System.out.println("s  " + convertToInt(s));
        
        return "Dias : "+days + "|  Horas : "+convertToInt(h) + "|  Minutos : "+convertToInt(m)+"|  Segundos : "+convertToInt(s);
    }
    
    private int toSeconds(int h, int m, int s) {
        return h * 3600 + m * 60 + s;
    }
    
    private int convertToInt(double value){
        return (int)(value);
    }
      
}
