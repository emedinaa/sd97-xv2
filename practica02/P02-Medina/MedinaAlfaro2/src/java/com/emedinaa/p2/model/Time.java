/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emedinaa.p2.model;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author edmedina
 */
@Named(value = "time")
@ApplicationScoped
public class Time {
    
    int hFirst, mFirst, sFirst;
    int hSecond, mSecond, sSecond;

    public int gethFirst() {
        return hFirst;
    }

    public void sethFirst(int hFirst) {
        this.hFirst = hFirst;
    }

    public int getmFirst() {
        return mFirst;
    }

    public void setmFirst(int mFirst) {
        this.mFirst = mFirst;
    }

    public int getsFirst() {
        return sFirst;
    }

    public void setsFirst(int sFirst) {
        this.sFirst = sFirst;
    }

    public int gethSecond() {
        return hSecond;
    }

    public void sethSecond(int hSecond) {
        this.hSecond = hSecond;
    }

    public int getmSecond() {
        return mSecond;
    }

    public void setmSecond(int mSecond) {
        this.mSecond = mSecond;
    }

    public int getsSecond() {
        return sSecond;
    }

    public void setsSecond(int sSecond) {
        this.sSecond = sSecond;
    }
    
    
}
