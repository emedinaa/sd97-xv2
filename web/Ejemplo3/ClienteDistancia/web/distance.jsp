<%-- 
    Document   : distance
    Created on : Aug 27, 2020, 11:06:52 PM
    Author     : edmedina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cliente Distancia!</h1>
        <h3>Construya un servicio que calcule la distancia entre 2 puntos dados tal como P1 y P2.</h3>
        <form action="distanceServlet" method="POST">
	<label for="fname">Punto 1 X:</label><br>
	<input type="text" name="txtPunto1X" value="" /><br>
	<label for="fname">Punto 1 Y:</label><br>
        <input type="text" name="txtPunto1Y" value="" /><br>
        <label for="fname">Punto 2 X:</label><br>
	<input type="text" name="txtPunto2X" value="" /><br>
        <label for="fname">Punto 2 Y:</label><br>
        <input type="text" name="txtPunto2Y" value="" /><br>
	<input type="submit" name="btnEnviar" value="Enviar" />
        </form>
    </body>
</html>
