/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.distancia;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "distanciaWebService")
public class distanciaWebService {


    /**
     * Web service operation
     */
    @WebMethod(operationName = "calculateDistancia")
    public Double calculateDistancia(@WebParam(name = "punto1X") double punto1X, @WebParam(name = "punto1Y") double punto1Y,
            @WebParam(name = "punto2X") double punto2X,@WebParam(name = "punto2Y") double punto2Y) {
            Double dX = punto1X - punto2X;
            Double dY = punto1Y - punto2Y;
            
            Double distance = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
        return distance;
    }
}
