/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.cambio;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "cambioWebService")
public class cambioWebService {


    /**
     * Web service operation
     */
    @WebMethod(operationName = "calculateCambio")
    public Double calculateCambio(@WebParam(name = "dolares") double dolares, @WebParam(name = "euros") double euros) {
        final double SOLES = 3.34;
        final double DOLAR = 1.15;
        double euroSoles,dolareSoles;
        euroSoles = (euros/DOLAR)*SOLES;
        dolareSoles = dolares*SOLES;
        double total = euroSoles + dolareSoles;
        return total;
    }
}
