<%-- 
    Document   : cambio
    Created on : Aug 27, 2020, 10:34:24 PM
    Author     : edmedina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cliente Cambio!</h1>
         <h3>Se cuenta con dos cantidades de dinero en dólares y euros. Diseñe un servicio que determine el monto total del dinero en soles. Considere los siguientes tipos de cambio:</h3>
         <h3>1 dólar = 3.34 soles</h3>
         <h3>1 dólar = 1.15 euros</h3>
          <form action="cambioServlet" method="POST">
            <label for="fname">Dolares:</label><br>
            <input type="text" name="txtDolares" value="" /><br>
            <label for="fname">Euros :</label><br>
            <input type="text" name="txtEuros" value="" />
            <input type="submit" name="btnEnviar" value="Enviar" />
        </form>
    </body>
</html>
