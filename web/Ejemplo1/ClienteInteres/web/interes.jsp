<%-- 
    Document   : interes
    Created on : Aug 27, 2020, 9:57:45 PM
    Author     : edmedina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cliente Interes!</h1>
        <h3>Calcular el monto del interés que recibe un cliente de un banco por un depósito a plazo fijo, se sabe que por cada mes que se mantiene el depósito el cliente recibe un interés del 3%.</h3>
          <form action="interesServlet" method="POST">
            <label for="fname">Depósito :</label><br>
            <input type="text" name="txtDeposito" value="" /><br>
            <label for="fname">Meses :</label><br>
            <input type="text" name="txtMeses" value="" /><br>
            <input type="submit" name="btnEnviar" value="Enviar" />
        </form>
    </body>
</html>
