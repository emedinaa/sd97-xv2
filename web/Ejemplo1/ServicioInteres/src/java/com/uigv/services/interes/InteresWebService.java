/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.interes;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "InteresWebService")
public class InteresWebService {


    /**
     * Web service operation
     */
    @WebMethod(operationName = "calculateInteres")
    public Double calculateInteres(@WebParam(name = "deposito") double deposito, @WebParam(name = "meses") int meses) {
        Double resultado = 0.03*deposito*meses;
        return resultado;
    }
}
