/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.esfera;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "VolumenEsfera")
public class VolumenEsfera {


    /**
     * Web service operation
     */
    @WebMethod(operationName = "calculateVolumen")
    public Double calculateVolumen(@WebParam(name = "radio") double radio) {
        return (4.0/3.0)*Math.PI*Math.pow(radio,3);
    }
}
