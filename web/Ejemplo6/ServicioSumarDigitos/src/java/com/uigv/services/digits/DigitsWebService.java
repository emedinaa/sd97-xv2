/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.digits;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "DigitsWebService")
public class DigitsWebService {

      /**
     * 
    //Probar este código es un compilador online https://www.jdoodle.com/online-java-compiler/
    public class MyClass {
        public static void main(String args[]) {
         int total = 0;
         int digit = 203;

            while(digit>0) {
               total += digit%10;
               digit = digit/10;
            }

            System.out.println("Sum of digits " + total);
        }
    }
    //Sum of digits 5 
     */
    

    /**
     * Web service operation
     */
    @WebMethod(operationName = "sumDigits")
    public int sumDigits(@WebParam(name = "number") int number) {
        int total = 0;
        int digit = number;
        
        while(digit>0) {
            total += digit%10;
            digit = digit/10;
        }
        return total;
    }
}
