<%-- 
    Document   : digits
    Created on : Aug 28, 2020, 3:58:36 PM
    Author     : edmedina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cliente Sumar Dígitos</h1>
        <h3>Diseñe un servicio que lea un número entero y determine la suma de sus cifras.</h3>
        <form action="sumDigitsServlet" method="POST">
            <label for="fname">Ingresar un número : </label><br>
            <input type="text" name="txtNumber" value="" /><br>
            <input type="submit" name="btnEnviar" value="Enviar" />
        </form>
    </body>
</html>
