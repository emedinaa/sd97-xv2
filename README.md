# SD97-XV2

Curso de Sistemas Distribuidos SD97-XV2 

NetBeans IDE 8.2 https://netbeans.org/downloads/old/8.2/start.html?platform=macosx&lang=en&option=javaee

## Web
```
web/
	Ejemplo1/
		- ClienteInteres
		- ServicioInteres
	Ejemplo2/
		- ClientCambio
		- ServicioCambio
	Esfera/
		- ClientEsfera
		- ServicioEsfera
	Ejemplo3/
		- ClientDistancia
		- ServicioDistancia
	Ejemplo6/
		- ClienteSumarDigitos
		- ServicioSumarDigitos
```

- Ejemplo 1 : Calcular interés
- Ejemplo 2 : Calcular cambio
- Ejemplo 3 : Calcular distancia entre 2 números
- Ejemplo 6 : Sumar dígitos
- Esfera : Calcular volumen de la esfera


## Desktop

- CuentaBancaria : Ejemplo de profesora
- DEjemplo2 : Calculadora
- Impuesto : Ejemplo de impuesto

## Práctica 01

- CasaCambio : Solución práctica 01, aplicación desktop

### Screenshots

![p 1](screenshots/practica01/screenshot1.png) ![p 2](screenshots/practica01/screenshot2.png)
![p 3](screenshots/practica01/screenshot3.png) ![p 4](screenshots/practica01/screenshot4.png)
![p 5](screenshots/practica01/screenshot5.png) ![p 6](screenshots/practica01/screenshot6.png)

![w 1](screenshots/web/screenshot1.png) ![w 2](screenshots/web/screenshot2.png)
![w 3](screenshots/web/screenshot3.png) ![w 4](screenshots/web/screenshot4.png)
![w 5](screenshots/web/screenshot5.png) ![w 6](screenshots/web/screenshot6.png)
![w 7](screenshots/web/screenshot7.png) ![w 8](screenshots/web/screenshot8.png)
![w 8](screenshots/web/screenshot8.png) ![w 9](screenshots/web/screenshot9.png)
![w 10](screenshots/web/screenshot10.png)