/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.calculator;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "calculatorWebService")
public class calculatorWebService {

    private double sum(int p1, int p2){
        return p1 + p2;
    }
    
    private double substract(int p1, int p2){
        return p1 - p2;
    }
    
    private double multiply(int p1, int p2){
        return p1 * p2;
    }
    
    private double divide(int p1, int p2){
        return (p1*1.0)/ p2;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "calculate")
    public Double calculate(@WebParam(name = "param1") int param1, @WebParam(name = "param2") int param2, @WebParam(name = "operation") int operation) {
        double resultado = 0;
        switch(operation){
            case 0 :
                resultado = sum(param1,param2);
                break;
            case 1 :
                resultado = substract(param1,param2);
                break;
            case 2 :
                resultado = multiply(param1,param2);
                break;
            case 3 :
                resultado = divide(param1,param2);
                break;
        }
        return resultado;
    }
}
