/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.impuesto;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "ImpuestoWebService")
public class ImpuestoWebService {

    final double IMP_RESTAURANTE = 5.60;
    final double IMP_ZAPATERIA = 4.20;
    final double IMP_BODEGA = 3.10;
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "calculateImpuesto")
    public Double calculateImpuesto(@WebParam(name = "rubro") int rubro, @WebParam(name = "area") double area) {
        double total = 0;
        double impuestoM = 0;
        switch(rubro){
            case 0 :
                impuestoM = IMP_RESTAURANTE;
                break;
            case 1 :
                impuestoM = IMP_ZAPATERIA;
                break;
            case 2 :
                impuestoM = IMP_BODEGA;
                break;
        }
        total = impuestoM* area;
        
        return total;
    }
}
