/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete_servicio;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Yuliana
 */
@WebService(serviceName = "Transacciones")
public class Transacciones {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "realizarDeposito")
    public Double realizarDeposito(@WebParam(name = "monto") double monto, @WebParam(name = "saldo") double saldo) {
        saldo=saldo+monto;
        return saldo;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "realizarretiro")
    public Double realizarretiro(@WebParam(name = "monto") double monto, @WebParam(name = "saldo") double saldo) {
        if(monto>saldo)
            return -1.0;
        else{
            saldo=saldo-monto;
            return saldo;
        }
    }
}
