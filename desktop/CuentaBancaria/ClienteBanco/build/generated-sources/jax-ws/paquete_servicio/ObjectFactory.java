
package paquete_servicio;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the paquete_servicio package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RealizarDepositoResponse_QNAME = new QName("http://paquete_servicio/", "realizarDepositoResponse");
    private final static QName _RealizarDeposito_QNAME = new QName("http://paquete_servicio/", "realizarDeposito");
    private final static QName _Realizarretiro_QNAME = new QName("http://paquete_servicio/", "realizarretiro");
    private final static QName _RealizarretiroResponse_QNAME = new QName("http://paquete_servicio/", "realizarretiroResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: paquete_servicio
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RealizarDeposito }
     * 
     */
    public RealizarDeposito createRealizarDeposito() {
        return new RealizarDeposito();
    }

    /**
     * Create an instance of {@link Realizarretiro }
     * 
     */
    public Realizarretiro createRealizarretiro() {
        return new Realizarretiro();
    }

    /**
     * Create an instance of {@link RealizarretiroResponse }
     * 
     */
    public RealizarretiroResponse createRealizarretiroResponse() {
        return new RealizarretiroResponse();
    }

    /**
     * Create an instance of {@link RealizarDepositoResponse }
     * 
     */
    public RealizarDepositoResponse createRealizarDepositoResponse() {
        return new RealizarDepositoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarDepositoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paquete_servicio/", name = "realizarDepositoResponse")
    public JAXBElement<RealizarDepositoResponse> createRealizarDepositoResponse(RealizarDepositoResponse value) {
        return new JAXBElement<RealizarDepositoResponse>(_RealizarDepositoResponse_QNAME, RealizarDepositoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarDeposito }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paquete_servicio/", name = "realizarDeposito")
    public JAXBElement<RealizarDeposito> createRealizarDeposito(RealizarDeposito value) {
        return new JAXBElement<RealizarDeposito>(_RealizarDeposito_QNAME, RealizarDeposito.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Realizarretiro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paquete_servicio/", name = "realizarretiro")
    public JAXBElement<Realizarretiro> createRealizarretiro(Realizarretiro value) {
        return new JAXBElement<Realizarretiro>(_Realizarretiro_QNAME, Realizarretiro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarretiroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paquete_servicio/", name = "realizarretiroResponse")
    public JAXBElement<RealizarretiroResponse> createRealizarretiroResponse(RealizarretiroResponse value) {
        return new JAXBElement<RealizarretiroResponse>(_RealizarretiroResponse_QNAME, RealizarretiroResponse.class, null, value);
    }

}
