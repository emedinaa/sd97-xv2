/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.client.casacambio;

import com.uigv.services.operaciones.OperacionesWebService;
import com.uigv.services.operaciones.OperacionesWebService_Service;
import java.util.Date;

/**
 *
 * @author edmedina
 */
public class MainController {
    
    static final int COMPRA_OP = 1 ;
    static final int VENTA_OP = 2 ;
    
    static final int DOLARES = 1 ;
    static final int EUROS = 2 ;
    
    private final OperacionesWebService_Service  service = new OperacionesWebService_Service();
    private final OperacionesWebService operations;
    
    private final MainView mainView;

    public MainController(MainView mainView) {
        this.mainView = mainView;
        operations = service.getOperacionesWebServicePort();
    }
    
    private void addOperation(double monto, int operation, int moneda,boolean exchange){
        String exMoneda = getMoneda(moneda);
        if(exchange){
            exMoneda = "Soles";
        }
        double tCambio = getTCambioByOperationAndMoneda(operation,moneda);
        Object[] item={getOperation(operation), currentDate(),getMoneda(moneda), tCambio,monto,exMoneda};
        mainView.updateTable(item);
    }
    
    private String getMoneda(int moneda){
        return (moneda == DOLARES) ?"Dolares" : "Euros";
    }
    
    private String getOperation(int moneda){
        return (moneda == COMPRA_OP) ?"Compra" : "Venta";
    }
    
    private Date currentDate(){
        return new Date();
    }
    
    private double getTCambioByOperationAndMoneda(int operation, int moneda){
        double tcCambio =0;
        if(moneda == DOLARES){
            switch(operation){
                case COMPRA_OP :
                    tcCambio = operations.obtenerTipoCambio(DOLARES, COMPRA_OP);
                    break;
                case VENTA_OP :
                    tcCambio = operations.obtenerTipoCambio(DOLARES, VENTA_OP);
                    break;
            }
        }else if(moneda == EUROS){
            switch(operation){
                case COMPRA_OP :
                        tcCambio = operations.obtenerTipoCambio(EUROS, COMPRA_OP);
                    break;
                case VENTA_OP :
                        tcCambio = operations.obtenerTipoCambio(EUROS, VENTA_OP);
                    break;
            }
        }
        return tcCambio;
    }
    
    public void getTCambioDolares(){
        double tcCompra = operations.obtenerTipoCambio(DOLARES, COMPRA_OP);
        double tcVenta = operations.obtenerTipoCambio(DOLARES, VENTA_OP);
        mainView.renderClienteTCDolares(tcCompra, tcVenta);
    }
    
    public void getTCambioEuros(){
        double tcCompra = operations.obtenerTipoCambio(EUROS, COMPRA_OP);
        double tcVenta = operations.obtenerTipoCambio(EUROS, VENTA_OP);
        mainView.renderClienteTCEuros(tcCompra, tcVenta);
    }
    
    public void updateTCCompraDolares(double tCambio){
        operations.actualizarTipoCambio(tCambio, COMPRA_OP, DOLARES);
        mainView.showMessage("Operación completada!");
        getTCambioDolares();
    }
    
    public void updateTCVentaDolares(double tCambio){
        operations.actualizarTipoCambio(tCambio, VENTA_OP, DOLARES);
        mainView.showMessage("Operación completada!");
        getTCambioDolares();
    }
    
    public void updateTCCompraEuros(double tCambio){
        operations.actualizarTipoCambio(tCambio, COMPRA_OP, EUROS);
        mainView.showMessage("Operación completada!");
        getTCambioEuros();
    }
    
    public void updateTCVentaEuros(double tCambio){
        operations.actualizarTipoCambio(tCambio,VENTA_OP, EUROS);
        mainView.showMessage("Operación completada!");
        getTCambioEuros();
    }
    
    public void eurosChange(double monto,int operation,boolean exchange){
        addOperation(monto,operation,EUROS,exchange);
        double result = 0;
        if(operation == COMPRA_OP){
            if(!exchange){
                 result= operations.compra(monto, EUROS);
            }else{
                result= operations.compraSoles(monto, EUROS);
            }
           
        }else if(operation == VENTA_OP){
            if(!exchange){
                result= operations.venta(monto, EUROS);
            }else{
             result= operations.ventaSoles(monto, EUROS);
            }
        }
        mainView.showMessage("Operación completada!");
        mainView.showResultEuros(result);
    }
    
    public void dolaresChange(double monto,int operation,boolean exchange){
        addOperation(monto,operation,DOLARES,exchange);
        double result = 0;
        if(operation == COMPRA_OP){
            if(!exchange){
                result= operations.compra(monto, DOLARES);
            }else{
                 result= operations.compraSoles(monto, DOLARES);
            }
        }else if(operation == VENTA_OP){
             if(!exchange){
                result= operations.venta(monto, DOLARES);
             }else{
                 result= operations.ventaSoles(monto, DOLARES);
             }
        }
        mainView.showMessage("Operación completada!");
        mainView.showResultDolares(result);
    }
}
