/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.client.casacambio;

/**
 *
 * @author edmedina
 */
public interface MainView {
    
    
    public void updateTable(Object[] item);
    
    public void showMessage(String message);
    
    public void showResultDolares(double result);
    public void showResultEuros(double result);
        
    public void renderClienteTCDolares(double tCambioCompra,double tCambioVenta );
    
    public void renderClienteTCEuros(double tCambioCompra,double tCambioVenta );
}
