
package com.uigv.services.operaciones;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.uigv.services.operaciones package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VentaSolesResponse_QNAME = new QName("http://operaciones.services.uigv.com/", "ventaSolesResponse");
    private final static QName _VentaResponse_QNAME = new QName("http://operaciones.services.uigv.com/", "ventaResponse");
    private final static QName _VentaSoles_QNAME = new QName("http://operaciones.services.uigv.com/", "ventaSoles");
    private final static QName _CompraSolesResponse_QNAME = new QName("http://operaciones.services.uigv.com/", "compraSolesResponse");
    private final static QName _ObtenerTipoCambioResponse_QNAME = new QName("http://operaciones.services.uigv.com/", "obtenerTipoCambioResponse");
    private final static QName _Compra_QNAME = new QName("http://operaciones.services.uigv.com/", "compra");
    private final static QName _CompraSoles_QNAME = new QName("http://operaciones.services.uigv.com/", "compraSoles");
    private final static QName _ObtenerTipoCambio_QNAME = new QName("http://operaciones.services.uigv.com/", "obtenerTipoCambio");
    private final static QName _Venta_QNAME = new QName("http://operaciones.services.uigv.com/", "venta");
    private final static QName _ActualizarTipoCambio_QNAME = new QName("http://operaciones.services.uigv.com/", "actualizarTipoCambio");
    private final static QName _ActualizarTipoCambioResponse_QNAME = new QName("http://operaciones.services.uigv.com/", "actualizarTipoCambioResponse");
    private final static QName _CompraResponse_QNAME = new QName("http://operaciones.services.uigv.com/", "compraResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.uigv.services.operaciones
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerTipoCambioResponse }
     * 
     */
    public ObtenerTipoCambioResponse createObtenerTipoCambioResponse() {
        return new ObtenerTipoCambioResponse();
    }

    /**
     * Create an instance of {@link Compra }
     * 
     */
    public Compra createCompra() {
        return new Compra();
    }

    /**
     * Create an instance of {@link CompraSoles }
     * 
     */
    public CompraSoles createCompraSoles() {
        return new CompraSoles();
    }

    /**
     * Create an instance of {@link ObtenerTipoCambio }
     * 
     */
    public ObtenerTipoCambio createObtenerTipoCambio() {
        return new ObtenerTipoCambio();
    }

    /**
     * Create an instance of {@link Venta }
     * 
     */
    public Venta createVenta() {
        return new Venta();
    }

    /**
     * Create an instance of {@link ActualizarTipoCambio }
     * 
     */
    public ActualizarTipoCambio createActualizarTipoCambio() {
        return new ActualizarTipoCambio();
    }

    /**
     * Create an instance of {@link ActualizarTipoCambioResponse }
     * 
     */
    public ActualizarTipoCambioResponse createActualizarTipoCambioResponse() {
        return new ActualizarTipoCambioResponse();
    }

    /**
     * Create an instance of {@link CompraResponse }
     * 
     */
    public CompraResponse createCompraResponse() {
        return new CompraResponse();
    }

    /**
     * Create an instance of {@link VentaSolesResponse }
     * 
     */
    public VentaSolesResponse createVentaSolesResponse() {
        return new VentaSolesResponse();
    }

    /**
     * Create an instance of {@link VentaResponse }
     * 
     */
    public VentaResponse createVentaResponse() {
        return new VentaResponse();
    }

    /**
     * Create an instance of {@link VentaSoles }
     * 
     */
    public VentaSoles createVentaSoles() {
        return new VentaSoles();
    }

    /**
     * Create an instance of {@link CompraSolesResponse }
     * 
     */
    public CompraSolesResponse createCompraSolesResponse() {
        return new CompraSolesResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VentaSolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "ventaSolesResponse")
    public JAXBElement<VentaSolesResponse> createVentaSolesResponse(VentaSolesResponse value) {
        return new JAXBElement<VentaSolesResponse>(_VentaSolesResponse_QNAME, VentaSolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "ventaResponse")
    public JAXBElement<VentaResponse> createVentaResponse(VentaResponse value) {
        return new JAXBElement<VentaResponse>(_VentaResponse_QNAME, VentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VentaSoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "ventaSoles")
    public JAXBElement<VentaSoles> createVentaSoles(VentaSoles value) {
        return new JAXBElement<VentaSoles>(_VentaSoles_QNAME, VentaSoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompraSolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "compraSolesResponse")
    public JAXBElement<CompraSolesResponse> createCompraSolesResponse(CompraSolesResponse value) {
        return new JAXBElement<CompraSolesResponse>(_CompraSolesResponse_QNAME, CompraSolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerTipoCambioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "obtenerTipoCambioResponse")
    public JAXBElement<ObtenerTipoCambioResponse> createObtenerTipoCambioResponse(ObtenerTipoCambioResponse value) {
        return new JAXBElement<ObtenerTipoCambioResponse>(_ObtenerTipoCambioResponse_QNAME, ObtenerTipoCambioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Compra }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "compra")
    public JAXBElement<Compra> createCompra(Compra value) {
        return new JAXBElement<Compra>(_Compra_QNAME, Compra.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompraSoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "compraSoles")
    public JAXBElement<CompraSoles> createCompraSoles(CompraSoles value) {
        return new JAXBElement<CompraSoles>(_CompraSoles_QNAME, CompraSoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerTipoCambio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "obtenerTipoCambio")
    public JAXBElement<ObtenerTipoCambio> createObtenerTipoCambio(ObtenerTipoCambio value) {
        return new JAXBElement<ObtenerTipoCambio>(_ObtenerTipoCambio_QNAME, ObtenerTipoCambio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Venta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "venta")
    public JAXBElement<Venta> createVenta(Venta value) {
        return new JAXBElement<Venta>(_Venta_QNAME, Venta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarTipoCambio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "actualizarTipoCambio")
    public JAXBElement<ActualizarTipoCambio> createActualizarTipoCambio(ActualizarTipoCambio value) {
        return new JAXBElement<ActualizarTipoCambio>(_ActualizarTipoCambio_QNAME, ActualizarTipoCambio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarTipoCambioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "actualizarTipoCambioResponse")
    public JAXBElement<ActualizarTipoCambioResponse> createActualizarTipoCambioResponse(ActualizarTipoCambioResponse value) {
        return new JAXBElement<ActualizarTipoCambioResponse>(_ActualizarTipoCambioResponse_QNAME, ActualizarTipoCambioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompraResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://operaciones.services.uigv.com/", name = "compraResponse")
    public JAXBElement<CompraResponse> createCompraResponse(CompraResponse value) {
        return new JAXBElement<CompraResponse>(_CompraResponse_QNAME, CompraResponse.class, null, value);
    }

}
