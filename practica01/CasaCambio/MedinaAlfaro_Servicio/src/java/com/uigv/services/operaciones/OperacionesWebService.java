/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.services.operaciones;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author edmedina
 */
@WebService(serviceName = "OperacionesWebService")
public class OperacionesWebService {

   public final int OPERATION_COMPRA = 1; 
   public final int OPERATION_VENTA = 2;
   
   public final int MONEDA_DOLARES = 1; 
   public final int MONEDA_EUROS = 2;
   
   private double tipoCambioCompraDolares = 3.54;
   private double tipoCambioVentaDolares = 3.55;
      
   private double tipoCambioCompraEuros = 4.20;
   private double tipoCambioVentaEuros = 4.21;
   
       /**
     * Web service operation
     */
    @WebMethod(operationName = "compraSoles")
    public double compraSoles(@WebParam(name = "monto") double monto, @WebParam(name = "moneda") int moneda) {
        double result = 0;
        if(moneda==MONEDA_DOLARES){
            result = monto/tipoCambioCompraDolares;
        }else if(moneda ==MONEDA_EUROS ){
            result = monto/tipoCambioCompraEuros;
        }
        return result;
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "compra")
    public double compra(@WebParam(name = "monto") double monto, @WebParam(name = "moneda") int moneda) {
        double result = 0;
        if(moneda==MONEDA_DOLARES){
            result = monto*tipoCambioCompraDolares;
        }else if(moneda ==MONEDA_EUROS ){
            result = monto*tipoCambioCompraEuros;
        }
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "venta")
    public double venta(@WebParam(name = "monto") double monto, @WebParam(name = "moneda") int moneda) {
        double result = 0;
        if(moneda==MONEDA_DOLARES){
            result = monto*tipoCambioVentaDolares;
        }else if(moneda ==MONEDA_EUROS ){
            result = monto*tipoCambioVentaEuros;
        }
        return result;
    }
    
        /**
     * Web service operation
     */
    @WebMethod(operationName = "ventaSoles")
    public double ventaSoles(@WebParam(name = "monto") double monto, @WebParam(name = "moneda") int moneda) {
        double result = 0;
        if(moneda==MONEDA_DOLARES){
            result = monto/tipoCambioVentaDolares;
        }else if(moneda ==MONEDA_EUROS ){
            result = monto/tipoCambioVentaEuros;
        }
        return result;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "actualizarTipoCambio")
    public boolean actualizarTipoCambio(@WebParam(name = "tipoCambio") double tipoCambio, @WebParam(name = "operation") int operacion,
            @WebParam(name = "moneda") int moneda) {
        int op = -1;
        if(operacion == OPERATION_COMPRA){
             switch(moneda) {
                 case MONEDA_DOLARES :
                     tipoCambioCompraDolares = tipoCambio;
                     op = OPERATION_COMPRA;
                     break;
                 case MONEDA_EUROS :
                     tipoCambioCompraEuros = tipoCambio;
                     op = OPERATION_COMPRA;
                     break;
             }
        }else if(operacion == OPERATION_VENTA){
             switch(moneda) {
                 case MONEDA_DOLARES :
                     tipoCambioVentaDolares = tipoCambio;
                     op = OPERATION_VENTA;
                     break;
                 case MONEDA_EUROS :
                     tipoCambioVentaEuros = tipoCambio;
                     op = OPERATION_VENTA;
                     break;
             }
        }
        
        return op>0;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "obtenerTipoCambio")
    public double obtenerTipoCambio(@WebParam(name = "moneda") int moneda, @WebParam(name = "operation") int operacion) {
        double tipoCambio = 0;
        if(operacion == OPERATION_COMPRA){
             switch(moneda) {
                 case MONEDA_DOLARES :
                     tipoCambio = tipoCambioCompraDolares;
                     break;
                 case MONEDA_EUROS :
                     tipoCambio = tipoCambioCompraEuros;
                     break;
             }
        }else if(operacion == OPERATION_VENTA){
             switch(moneda) {
                 case MONEDA_DOLARES :
                     tipoCambio = tipoCambioVentaDolares;
                     break;
                 case MONEDA_EUROS :
                     tipoCambio = tipoCambioVentaEuros;
                     break;
             }
        }
        
        return tipoCambio;
    }
}
