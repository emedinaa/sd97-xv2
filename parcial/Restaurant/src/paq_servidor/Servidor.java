package paq_servidor;

import com.uigv.parcial.data.DishData;
import com.uigv.parcial.model.Order;
import com.uigv.parcial.utils.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor extends Observable implements Runnable{
    private final int puerto;
    private final List<Socket> socketList;

    private ServerSocket serverSocket;
    private Socket socket;
    private DataOutputStream dataOutputStream;

    public Servidor(int puerto) {
        this.socketList = new ArrayList<>();
        this.puerto = puerto;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(puerto);
            while(true){
                socket = serverSocket.accept();
                socketList.add(socket);
                sendData(socket);
                
                InputStream inputStream = socket.getInputStream();
                 
                try{
                    ObjectInputStream dishesObjectInputStream = new ObjectInputStream(inputStream);
                    Order order = (Order)dishesObjectInputStream.readObject();
                    Log.d("Servidor order "+order);

                    if(order !=null){
                        this.setChanged();
                        this.notifyObservers(order);
                        this.clearChanged(); 
                    } 
                }catch(ClassCastException e){
                    Log.d("Servidor Order error "+e);
                }
                
            }
        }catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            Log.d("Servidor run  error "+ex);
        }
    }
   
    private void sendData(Socket client){
         Log.d("Servidor sendData  "+client);
         try {
             DataOutputStream clientDataOutputStream=new DataOutputStream(client.getOutputStream());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientDataOutputStream);
             objectOutputStream.writeObject(DishData.getAll());
         } catch (IOException ex) {
             Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);   
               Log.d("Servidor sendData error "+ex);
         }
     }
    
}
