/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.parcial.data;

import com.uigv.parcial.model.Dish;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edmedina
 */
public class DishData {
    
    public static List<Dish> getAll(){
        List<Dish> data = new ArrayList();
        data.add(new Dish(100,"Arroz con Pollo","Plato arroz con pollo",14.0));
        data.add(new Dish(101,"Ceviche","Plato de ceviche",12.0));
        data.add(new Dish(102,"Lomo Saltado","Plato de Lomo Saltado",16.0));
        data.add(new Dish(103,"Ají de gallina","Plato de Ají de gallina",14.0));
        data.add(new Dish(104,"Causa limeña","Plato de Causa limeña",18.0));
        data.add(new Dish(105,"Pachamanca","Plato de Pachamanca",20.0));  
        data.add(new Dish(107,"Tallarines a la Huacaína","Plato de Tallarines a la Huacaína",17.0));  
        data.add(new Dish(108,"Tacu tacu","Plato de Tacu tacu",15.0));  
        data.add(new Dish(109,"Pollo a la brasa","Plato de pollo a la brasa",18.0));
        data.add(new Dish(110,"Aguadito","Plato de aguadito",14.0));
        return data;
    }
    
}
