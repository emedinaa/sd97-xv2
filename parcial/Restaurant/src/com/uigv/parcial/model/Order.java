/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uigv.parcial.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author edmedina
 */
public class Order implements Serializable {
    
    private int id;
    private String client;
    private List<Dish> dishes;
    private double total;

    public Order(int id, String client, List<Dish> dishes, double total) {
        this.id = id;
        this.client = client;
        this.dishes = dishes;
        this.total = total;
    }

  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
    
    
    
}
