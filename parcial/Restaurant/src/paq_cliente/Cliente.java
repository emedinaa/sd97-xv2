package paq_cliente;

import com.uigv.parcial.model.Dish;
import com.uigv.parcial.model.Order;
import com.uigv.parcial.utils.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import paq_servidor.Servidor;

public class Cliente extends Observable implements Runnable{
    private final int port;
    private final String host="127.0.0.1";
    private Socket socket;

    public Cliente(int port) {
        this.port = port;
    }
    
    @Override
    public void run() {
        int cod;
        String nom;
        double pre;
        try {
            socket=new Socket(host, port);
          
            while(true){ 
                InputStream inputStream = socket.getInputStream();
                
                /*ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                List<Producto> products = (List<Producto>)objectInputStream.readObject();*/
                try{
                    ObjectInputStream dishesObjectInputStream = new ObjectInputStream(inputStream);
                    List<Dish> dishes = (List<Dish>)dishesObjectInputStream.readObject();
                    
                    if(dishes!=null){
                       Log.d("dishes "+dishes);
                        this.setChanged();
                        this.notifyObservers(dishes);
                        this.clearChanged(); 
                    }
                }catch(ClassCastException e){
                      Log.d("dishes error "+e);
                }
                
              
                /*if(products!=null){
                    System.out.println("products "+products);
                    this.setChanged();
                    this.notifyObservers(products);
                    this.clearChanged(); 
                }*/
                
              
                
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            Log.d("run error "+ex);
        }
        
    }
    
     public void sendData(Order order){
        try {
            Log.d("sendData order "+order);
            DataOutputStream clientDataOutputStream=new DataOutputStream(socket.getOutputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientDataOutputStream);
            objectOutputStream.writeObject(order);
        } catch (IOException ex) {
             Log.d("sendData error "+ex);
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
