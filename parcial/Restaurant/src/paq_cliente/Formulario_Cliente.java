package paq_cliente;

import com.uigv.parcial.model.Dish;
import com.uigv.parcial.model.Order;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.DefaultTableModel;
import com.uigv.parcial.utils.Log;
import java.util.ArrayList;
import java.util.Date;

public class Formulario_Cliente extends javax.swing.JFrame implements Observer{
    private final Cliente client;
    private final DefaultTableModel defaultTableModel;
    private final DefaultTableModel orderDefaultTableModel;
    
    private List<Dish> dishOrderList = new ArrayList();
    private List<Dish> dishes = new ArrayList();
    
    public Formulario_Cliente() {
        initComponents();
        defaultTableModel=new DefaultTableModel();
        orderDefaultTableModel = new DefaultTableModel();
        showHeaders();
        
        client=new Cliente(2000);
        client.addObserver(this);
        Thread t=new Thread(client);
        t.start();
    }
    
    private void showHeaders(){
        defaultTableModel.addColumn("Id");
        defaultTableModel.addColumn("Nombre");
        defaultTableModel.addColumn("Precio");
        dishesJTable.setModel(defaultTableModel);
        
        orderDefaultTableModel.addColumn("Id");
        orderDefaultTableModel.addColumn("Nombre");
        orderDefaultTableModel.addColumn("Descripción");
        orderDefaultTableModel.addColumn("Precio");
        
        orderJTable.setModel(orderDefaultTableModel);
    }
    
    private void addDishToOrder(int row){
        if(dishes!=null && row<dishes.size()){
            Dish dish = dishes.get(row);
            Object[] item = {dish.getId(),dish.getName(),dish.getDesc(),dish.getPrice()};
            orderDefaultTableModel.addRow(item);
            if(dishOrderList!=null){
                 dishOrderList.add(dish);
            }
            renderTotal();
        }
    }
    
    private double getTotal(){
        double total = 0;
        if(dishOrderList!=null){
            for(Dish dish : dishOrderList){
                total += dish.getPrice();
            }
        }
        return total;
    }
    
    private void renderTotal(){       
        totalJTextField.setText("S./"+getTotal());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        dishesJTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        orderJTable = new javax.swing.JTable();
        usernameJTextField = new javax.swing.JTextField();
        sendJButton = new javax.swing.JButton();
        totalJTextField = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        cleanJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        dishesJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        dishesJTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dishesJTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(dishesJTable);

        orderJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        orderJTable.setName("orderJTable"); // NOI18N
        jScrollPane2.setViewportView(orderJTable);

        usernameJTextField.setName("usernameJTextField"); // NOI18N

        sendJButton.setText("Enviar Pedido");
        sendJButton.setName("sendJButton"); // NOI18N
        sendJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendJButtonActionPerformed(evt);
            }
        });

        totalJTextField.setName("amountJTextField"); // NOI18N

        cleanJButton.setText("Limpiar");
        cleanJButton.setName("cleanJButton"); // NOI18N
        cleanJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanJButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Username :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(usernameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(cleanJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(totalJTextField))
                                            .addComponent(sendJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(totalJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cleanJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(usernameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sendJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dishesJTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dishesJTableMouseClicked
        int row = dishesJTable.rowAtPoint(evt.getPoint());
        int col = dishesJTable.columnAtPoint(evt.getPoint());
        if (row >= 0 && col >= 0) {
           addDishToOrder(row);
        }
    }//GEN-LAST:event_dishesJTableMouseClicked

    private void cleanJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanJButtonActionPerformed
        clearOrderTable();
        totalJTextField.setText("S./0.0");
    }//GEN-LAST:event_cleanJButtonActionPerformed

    private void createOrder(String username){
        Log.d("createOrder "+ username );
        
        int unique_id= (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE); 
        Order order = new Order(unique_id,username,dishOrderList,getTotal());
        
         Log.d("createOrder order "+ order );
        client.sendData(order);
    }
    
    private void sendJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendJButtonActionPerformed
        String username = usernameJTextField.getText().trim();
        Log.d("sendJButtonActionPerformed "+ username +"dishOrderList "+dishOrderList.size() );
        if(username!=null && !username.isEmpty() && !dishOrderList.isEmpty()){
            createOrder(username);
        }
    }//GEN-LAST:event_sendJButtonActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Formulario_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Formulario_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Formulario_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Formulario_Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Formulario_Cliente().setVisible(true);
            }
        });
    }
    
    public void clearTable(){
        Log.d("clearTable");
        defaultTableModel.setRowCount(0);
    }
    
    public void clearOrderTable(){
        Log.d("clearOrderTable");
        orderDefaultTableModel.setRowCount(0);
        dishOrderList.clear();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cleanJButton;
    private javax.swing.JTable dishesJTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable orderJTable;
    private javax.swing.JButton sendJButton;
    private javax.swing.JTextField totalJTextField;
    private javax.swing.JTextField usernameJTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {  
        
        if(arg instanceof List){
            
            try{
                List<Dish> dishList = (List<Dish>)arg;
                if(dishList!=null){
                    for(Dish dish : dishList){
                        Object[] row = {dish.getId(),dish.getName(),dish.getPrice()};
                        defaultTableModel.addRow(row);
                    }
                    dishes = dishList;
                }
            }catch(ClassCastException e){
                Log.d("dishes e "+e)  ;
            }
        }
     
    }
}
