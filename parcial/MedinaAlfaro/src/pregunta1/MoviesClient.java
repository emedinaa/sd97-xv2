/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta1;

import com.uigv.parcial.utils.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import pregunta1.model.Movie;

/**
 * @author Eduardo Medina
 */
public class MoviesClient extends Observable implements Runnable{
    
    private final int port;
    private final String host="127.0.0.1";
    private Socket socket;

    public MoviesClient(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try{
         socket=new Socket(host, port);
          while(true){ 
                InputStream inputStream = socket.getInputStream();
                
                try{
                    ObjectInputStream dishesObjectInputStream = new ObjectInputStream(inputStream);
                    List<Movie> billboard = (List<Movie>)dishesObjectInputStream.readObject();
                    
                    if(billboard!=null){
                       Log.d("billboard "+billboard);
                        this.setChanged();
                        this.notifyObservers(billboard);
                        this.clearChanged(); 
                    }
                }catch(ClassCastException e){
                      Log.d("dishes error "+e);
                }
                
          }
        }catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(MoviesClient.class.getName()).log(Level.SEVERE, null, ex);
            Log.d("run error "+ex);
        }
    }
}
   
