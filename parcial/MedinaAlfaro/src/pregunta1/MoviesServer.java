/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta1;

import com.uigv.parcial.utils.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import pregunta1.model.Movie;

/**
 * @author Eduardo Medina
 */
public class MoviesServer  extends Observable implements Runnable{
    private final int port;
    private final List<Socket> socketList;

    private ServerSocket serverSocket;
    private Socket socket;
    private DataOutputStream dataOutputStream;

    public MoviesServer(int port) {
        this.socketList = new ArrayList<>();
        this.port = port;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            while(true){
                socket = serverSocket.accept();
                socketList.add(socket);
            }
        }catch (IOException ex) {
            Logger.getLogger(MoviesServer.class.getName()).log(Level.SEVERE, null, ex);
            Log.d("Servidor run  error "+ex);
        }
    }
    
    private void sendData(Socket client,List<Movie> data){
         Log.d("Server sendData  "+client);
         try {
             DataOutputStream clientDataOutputStream=new DataOutputStream(client.getOutputStream());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientDataOutputStream);
             objectOutputStream.writeObject(data);
         } catch (IOException ex) {
             Logger.getLogger(MoviesServer.class.getName()).log(Level.SEVERE, null, ex);   
               Log.d("Servidor sendData error "+ex);
         }
     }
    
    public void sendBillboard(List<Movie> data){
        Log.d("Server sendBillboard  "+data);
        for(Socket socket: socketList){
            sendData(socket,data);
        }
    }
}

