/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta1.model;

import java.io.Serializable;

/**
 *
 * @author Eduardo Medina
 */
public class Movie implements Serializable{
    
    private int id;
    private String name;
    private MovieCategory category;
    private String date;

    public Movie() {
    }

    
    public Movie(int id, String name, MovieCategory category, String date) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.date = date;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieCategory getCategory() {
        return category;
    }

    public void setCategory(MovieCategory category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
}
