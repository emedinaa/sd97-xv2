/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta1.model;

/**
 *
 * @author Eduardo Medina
 */
public enum MovieCategory {
    HORROR,
    COMEDY,
    ACTION,
    DRAMA,
    FANTASY,
    ROMANCE,
    THRILLER,
    SCIENCE_FICTION,
}
