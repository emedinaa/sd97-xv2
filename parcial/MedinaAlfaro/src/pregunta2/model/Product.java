/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta2.model;

import java.io.Serializable;

/**
 *
 * @author Eduardo Medina
 */
public class Product implements Serializable {
    
    private int id;
    private ProductType type;
    private double amount;

    public Product() {
    }
        
    public Product(int id, ProductType type, double amount) {
        this.id = id;
        this.type = type;
        this.amount = amount;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
}
