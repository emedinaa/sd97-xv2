/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta2.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Eduardo Medina
 */
public class Report implements Serializable {
    
    private String stationId;
    private String stationName;
    private List<Product> products;

    public Report() {
    }

    
    public Report(String stationId, String stationName, List<Product> products) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.products = products;
    }

    
    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    
    
    
}
