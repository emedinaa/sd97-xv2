/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta2;

import com.uigv.parcial.utils.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import pregunta2.model.Report;

/**
 * @author Eduardo Medina
 */
public class GasStationServer extends Observable implements Runnable{
    private final int port;
    private final List<Socket> socketList;

    private ServerSocket serverSocket;
    private Socket socket;
    private DataOutputStream dataOutputStream;

    public GasStationServer(int port) {
        this.socketList = new ArrayList<>();
        this.port = port;
    }

    @Override
    public void run() {
           try {
            serverSocket = new ServerSocket(port);
            while(true){
                socket = serverSocket.accept();
                socketList.add(socket);
               
                InputStream inputStream = socket.getInputStream();
                
                try{
                    ObjectInputStream dishesObjectInputStream = new ObjectInputStream(inputStream);
                    Report report = (Report)dishesObjectInputStream.readObject();
                    
                    if(report!=null){
                       Log.d("report "+report);
                        this.setChanged();
                        this.notifyObservers(report);
                        this.clearChanged(); 
                    }
                }catch(ClassCastException |ClassNotFoundException e){
                      Log.d("report error "+e);
                }
            }
        }catch (IOException ex ) {
            Logger.getLogger(GasStationServer.class.getName()).log(Level.SEVERE, null, ex);
            Log.d("Servidor run  error "+ex);
        }
    }
}
