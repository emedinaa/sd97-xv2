/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta2;

import com.uigv.parcial.utils.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import pregunta2.model.Report;

/**
 * @author Eduardo Medina
 */
public class GasStationClient extends Observable implements Runnable{
    private final int port;
    private final String host="127.0.0.1";
    private Socket socket;

    public GasStationClient(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        try {
            socket=new Socket(host, port);
            
        } catch (IOException ex) {
            Logger.getLogger(GasStationClient.class.getName()).log(Level.SEVERE, null, ex);
            Log.d("run error "+ex);
        }
    }

    void sendReport(Report report) {
        if(socket==null) return;
        
         try {
            Log.d("sendData report "+report);
            DataOutputStream clientDataOutputStream=new DataOutputStream(socket.getOutputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientDataOutputStream);
            objectOutputStream.writeObject(report);
        } catch (IOException ex) {
             Log.d("sendData error "+ex);
            Logger.getLogger(GasStationClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
}
