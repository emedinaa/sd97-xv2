Examen Parcial

 
I. Objetivo.
El alumno soluciona los ejercicios empleando sockets
 
II. Estructura.
Para el desarrollo del trabajo académico se debe crear un Proyecto con sus Apellidos y cree un paquete (pregunta1, pregunta2) para guardar la solución de cada pregunta.
 
III. Forma de presentación
Una vez resueltos los ejercicios planteados, comprimir el proyecto que contiene la solución de los ejercicios y adjuntarlo a la plataforma.
 
IV. Ejercicios
 
1. Implementar un socket en una cadena de cines, que permita enviar desde el servidor la actualización de las películas de la semana, indicando el nombre de la película, tipo de película (infantil, acción, comedia, terror, etc) y fecha de estreno.
 
2. Implementar un socket en una cadena de grifos, de manera que cada sucursal reporte a la central (servidor) el monto obtenido por la venta de cada uno de los 3 tipos de combustible que ofrecen (Gasolina 90, Gasolina 95 y Gasolina 97), así como el ingreso total obtenido.

-------------------------------------------------

MedinaAlfaro :
Proyecto con la solución del parcial

Restaurant :
Proyecto adicional

Screenshots :
Capturas de Pantalla

-------------------------------------------------

Eduardo José Medina Alfaro
408984790
a408984790@uigv.pe
